// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyB0YZuH9IgOyD_HBe6iDBJKczZd11Tgh6Q',
    authDomain: 'controle-if-4688.firebaseapp.com',
    databaseURL: 'https://controle-if-4688.firebaseio.com',
    projectId: 'controle-if-4688',
    storageBucket: 'controle-if-4688.appspot.com',
    messagingSenderId: '527607751490',
    appId: '1:527607751490:web:a04520247562fd512adedb',
    measurementId: 'G-L815XXSWCT'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
